let div = document.getElementById('alumnos')
let table = document.createElement('table')
table = div.appendChild(table)
let datos = ''


fetch('./datos.json')
    .then(response => response.json())
    .then(dato => {
        dato.estudiantes.forEach(e => {
            datos += `
            <tr>
            <td class="label">Nombre:</td>
            <td>${e.nombres}</td>
            <td class="label">Cédula:</td>
            <td>${e.cedula}</td>
            <td class="label">Dirección:</td>
            <td>${e.direccion}</td>
            <td class="label">Télefono:</td>
            <td>${e.telefono}</td>
            <td class="label">Correo:</td>
            <td>${e.correo}</td>
            <td class="label">Curso:</td>
            <td>${e.curso}</td>
            <td class="label">Paralelo:</td>
            <td>${e.paralelo}</td>
            </tr>`
        })
        table.innerHTML = datos
    })